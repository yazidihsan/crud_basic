import React from "react";

const Map = () => {
  const makanans = [
    {
      makanan: "Bakso",
      harga: 12000,
    },
    {
      makanan: "Soto",
      harga: 5000,
    },
    {
      makanan: "Mie Ayam",
      harga: 7000,
    },
    {
      makanan: "Ayam Geprek",
      harga: 13000,
    },
  ];
  const total_harga = makanans.reduce((val, makanan) => val + makanan.harga, 0);

  return (
    <div>
      <ul>
        <h2>Map Makanan</h2>

        {makanans.map((makanan, index) => (
          <li>
            {index + 1}-Makanan : {makanan.makanan}-Harga : {makanan.harga}
          </li>
        ))}
      </ul>
      <ul>
        <h2>Filter Harga makanan lebih dari 11.000</h2>
        {makanans
          .filter((makanan) => makanan.harga > 11000)
          .map((makanan, index) => (
            <li>
              {index + 1}-Makanan:{makanan.makanan}-Harga makanan:{" "}
              {makanan.harga}
            </li>
          ))}
      </ul>
      <h3>Total Harga : {total_harga}</h3>
    </div>
  );
};

export default Map;
