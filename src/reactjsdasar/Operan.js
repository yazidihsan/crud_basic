import React, { Component } from "react";

export default class Operan extends Component {
  render() {
    const { makanan, gantiMakanan } = this.props;
    return (
      <div>
        <h2>Hasil operean makanan : {makanan}</h2>
        <button onClick={() => gantiMakanan("Mie Ayam")}>
          Ganti operan makanan
        </button>
      </div>
    );
  }
}
