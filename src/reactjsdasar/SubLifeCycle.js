import React, { Component } from "react";

export default class SubLifeCycle extends Component {
  componentWillUnmount() {
    this.props.ubahMakanan("Satai");
  }

  render() {
    return (
      <div>
        <h2>Tampil Sub Component</h2>
      </div>
    );
  }
}
