import React, { Component } from "react";
import SubLifeCycle from "./SubLifeCycle";

export default class Lifecycle extends Component {
  constructor(props) {
    super(props);

    this.state = {
      makanan: "Bakso",
      data: {},
      tampilSub: false,
    };
  }

  componentDidMount() {
    fetch("https://jsonplaceholder.typicode.com/todos/1")
      .then((response) => response.json())
      .then((json) =>
        this.setState({
          data: json,
        })
      );
  }

  ubahMakanan = (value) => {
    this.setState({
      makanan: value,
    });
  };

  render() {
    return (
      <div>
        <h2>{this.state.makanan}</h2>
        {this.state.tampilSub && (
          <SubLifeCycle ubahMakanan={(value) => this.ubahMakanan(value)} />
        )}
        <button
          onClick={() => this.setState({ tampilSub: !this.state.tampilSub })}
        >
          Tampil Sub Lifecycle
        </button>
      </div>
    );
  }
}
