import React from "react";

// Const
// const harga = 30000;
// if(true){
//   const  harga = 12000;
// }

// Var
// var harga = 20000;
// if(true){
//      var harga = 15000
// }

// Let
let harga = 23000;
if (true) {
  harga = 3000;
}
const Variabel = () => {
  return (
    <div>
      <h2>Harga : {harga}</h2>
    </div>
  );
};

export default Variabel;
