import React, { Component } from "react";
import Tabel from "./Tabel";
import NavBarComponent from "./NavBarComponent";
import Formulir from "./Formulir";

export default class Crud extends Component {
  constructor(props) {
    super(props);

    this.state = {
      makanans: [],
      nama: "",
      deskripsi: "",
      harga: 0,
      id: "",
    };
  }
  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };
  handleSubmit = (event) => {
    event.preventDefault();

    if (this.state.id === "") {
      this.setState({
        makanans: [
          ...this.state.makanans,
          {
            id: this.state.makanans.length + 1,
            nama: this.state.nama,
            deskripsi: this.state.deskripsi,
            harga: this.state.harga,
          },
        ],
      });
    } else {
      const makananTidakDipilih = this.state.makanans
        .filter((makanan) => makanan.id !== this.state.id)
        .map((filterMakanan) => {
          return filterMakanan;
        });

      this.setState({
        makanans: [
          ...makananTidakDipilih,
          {
            id: this.state.makanans.length + 1,
            nama: this.state.nama,
            deskripsi: this.state.deskripsi,
            harga: this.state.harga,
          },
        ],
      });
    }

    this.setState({
      nama: "",
      deskripsi: "",
      harga: 0,
      id: "",
    });
  };

  editMakanan = (id) => {
    const makananDipilih = this.state.makanans
      .filter((makanan) => makanan.id === id)
      .map((filterMakanan) => {
        return filterMakanan;
      });
    this.setState({
      nama: makananDipilih[0].nama,
      deskripsi: makananDipilih[0].deskripsi,
      harga: makananDipilih[0].harga,
      id: makananDipilih[0].id,
    });
  };

  hapusMakanan = (id) => {
    const makananBaru = this.state.makanans
      .filter((makanan) => makanan.id !== id)
      .map((filterMakanan) => {
        return filterMakanan;
      });
    this.setState({
      makanans: makananBaru,
    });
  };

  render() {
    console.log("Data", this.state.makanans);
    return (
      <div>
        <NavBarComponent />
        <div className="container mt-5">
          <Tabel
            makanans={this.state.makanans}
            editMakanan={this.editMakanan}
            hapusMakanan={this.hapusMakanan}
          />
          <Formulir
            {...this.state}
            handleChange={this.handleChange}
            handleSubmit={this.handleSubmit}
          />
        </div>
      </div>
    );
  }
}

// const initialState = {
//   makanans: [],
//   nama: "",
//   deskripsi: "",
//   harga: 0,
//   id: "",
// };
// const Crud = () => {
//   const [state, setState] = useState(initialState);

//   const handleChange = (event) => {
//     setState(prevState=>({...prevState,
//       [event.target.name]: event.target.value,
//     }));
//   };

//   const handleSubmit = (event) => {
//     event.preventDefault();

//     if (state.id === "") {
//       setState({
//         makanans: [
//           ...state.makanans,
//           setState(
//           {
//             id: state.makanans.length + 1,
//             nama: state.nama,
//             deskripsi: state.deskripsi,
//             harga: state.harga,
//           }),
//         ],
//       });
//     } else {
//       const makananTidakDipilih = state.makanans
//         .filter((makanan) => makanan.id !== state.id)
//         .map((filterMakanan) => {
//           return filterMakanan;
//         });

//       setState({
//         makanans: [
//           ...makananTidakDipilih,
//           setState(
//           {
//             id: state.makanans.length + 1,
//             nama: state.nama,
//             deskripsi: state.deskripsi,
//             harga: state.harga,
//           }),
//         ],
//       });
//     }

//     setState({
//       ...state,
//       nama: "",
//       deskripsi: "",
//       harga: 0,
//       id: "",
//     });
//   };

//   const editMakanan = (id) => {
//     const makananDipilih = state.makanans
//       .filter((makanan) => makanan.id === id)
//       .map((filterMakanan) => {
//         return filterMakanan;
//       });
//     setState({
//       ...state,
//       nama: makananDipilih[0].nama,
//       deskripsi: makananDipilih[0].deskripsi,
//       harga: makananDipilih[0].harga,
//       id: makananDipilih[0].id,
//     });
//   };

//   const hapusMakanan = (id) => {
//     const makananBaru = state.makanans
//       .filter((makanan) => makanan.id !== id)
//       .map((filterMakanan) => {
//         return filterMakanan;
//       });
//     setState({
//       ...state,
//       makanans: makananBaru,
//     });
//   };

//   return (
//     <div>
//       <NavBarComponent />
//       <div className="container mt-5">
//         <Tabel
//           makanans={state.makanans}
//           editMakanan={editMakanan}
//           hapusMakanan={hapusMakanan}
//         />
//         <Formulir
//           {...state}
//           handleChange={handleChange}
//           handleSubmit={handleSubmit}
//         />
//       </div>
//     </div>
//   );
// };
// export default Crud;
